﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoProjekt
{
    public static class ImageExtensions
    {
        static double NeighbourWeight = 0.1;
        static double CenterWeight = 0.6;
        public static byte CalculateFilter(this Image<Rgb24> image, Cord center, List<Cord> neighbours)
        {
            var avg = 0.0; 
            foreach(var pixel in neighbours)
            {
                avg+=(int)image[pixel.Y, pixel.X].B * NeighbourWeight;
            }

            avg += (int)image[center.Y, center.X].B * CenterWeight;

            return (byte)avg;
        }
    }
}
