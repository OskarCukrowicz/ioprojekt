﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IoProjekt
{
    public class Parallel
    {
        private Image<Rgb24> _image { get; set; }

        private Image<Rgb24> _output { get; set; }
        public Parallel(Image<Rgb24> image)
        {
            _image = image;
            _output = new Image<Rgb24>(image.Width, image.Height);
        }

        public Image<Rgb24> ApplyFilter()
        {
            Task task1 = Task.Factory.StartNew(() => IterateOverImage(0));
            Task task2 = Task.Factory.StartNew(() => IterateOverImage(1));
            Task task3 = Task.Factory.StartNew(() => IterateOverImage(2));
            Task task4 = Task.Factory.StartNew(() => IterateOverImage(3));

            Task.WaitAll(task1, task2, task3, task4);

            return _output;
        }

        private void IterateOverImage(int startingPoint, int stepSize = 4)
        {
            for(int i = startingPoint; i<_image.Height;i+=stepSize)
            {
                for(int j=0;j<_image.Width;j++)
                {
                    if (i >= _image.Height)
                    {
                        return;
                    }

                    var neigbours = ImageHelper.GetNeighbours(new Cord(i, j), _image.Height, _image.Width);

                    var blue = _image.CalculateFilter(new Cord(i, j), neigbours);

                    _output[j, i] = new Rgb24(0, 0, blue);
                }
            }
        }

    }
}
