﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoProjekt
{
    public class Cord
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Cord(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
    public static class ImageHelper
    {
        public static List<Cord> GetNeighbours(Cord main, int height, int width)
        {
            var neighbours = new List<Cord>();



            if (main.X + 1 < height)
            {
                neighbours.Add(new Cord(main.X + 1, main.Y));
            }
            if (main.X - 1 >= 0)
            {
                neighbours.Add(new Cord(main.X - 1, main.Y));
            }

            if (main.Y + 1 < width )
            {
                neighbours.Add(new Cord(main.X, main.Y + 1));
            }
            if (main.Y - 1 >= 0)
            {
                neighbours.Add(new Cord(main.X, main.Y - 1));
            }


            return neighbours;
        }



    }
}
