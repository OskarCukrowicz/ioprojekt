﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System;
using System.Diagnostics;
using System.IO;

namespace IoProjekt
{
    class Program
    {


        static Image<Rgb24> Sekwencyjna(Image<Rgb24> image)
        {
            var newImage = new Image<Rgb24>(image.Width, image.Height);

            for (int i = 0; i < image.Height; i++)
            {
                for (int j = 0; j < image.Width; j++)
                {
                    var neigbours = ImageHelper.GetNeighbours(new Cord(i, j), image.Height, image.Width);

                    var blue = image.CalculateFilter(new Cord(i, j), neigbours);

                    newImage[j, i] = new Rgb24(0, 0, blue);
                }
            }

            return newImage;
        }

        static Image<Rgb24> Wspolbiezna(Image<Rgb24> image)
        {
            var filter = new Parallel(image);
            return filter.ApplyFilter();
        }

        static void Main(string[] args)
        {
            var image = Image.Load<Rgb24>("test.jpg");

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            Wspolbiezna(image);
            stopwatch.Stop();

            var parallel = stopwatch.ElapsedMilliseconds;

            stopwatch.Reset();

            stopwatch.Start();
            Sekwencyjna(image);
            stopwatch.Stop();

            Console.WriteLine($"Przyspieszenie: {stopwatch.ElapsedMilliseconds/parallel}");
            Console.ReadLine();




            image.Save("after.jpg");
        }
    }
}
